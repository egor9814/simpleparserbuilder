package ru.egor9814.spb.builder.fsm

/**
 * Deterministic finite automata
 * @param alphabet ∑
 * @param states States of automata
 * */
data class DFA(
    val alphabet: Set<Char>,
    val states: Set<State>
) {

    /**
     * Convert DFA to transition table
     * @param out Special writer
     * @param skippedTokens Tokens for skipping in lexical analysis
     */
    internal fun write(out: DFAWriter, skippedTokens: Set<String>) {
        // Key: Current state
        // Val:
        //   Key: Target state
        //   Val: Set of char for transition from 'Current state' to 'Target state'
        val transitions = mutableMapOf<Int, Map<Int, Set<Char>>>()
        for (state in states) {
            val stateTransitions = mutableMapOf<Int, MutableSet<Char>>()
            for (c in alphabet) {
                state.getTransitions(c).apply {
                    if (isNotEmpty()) {
                        first().id.also {
                            stateTransitions[it]?.add(c) ?: stateTransitions.put(it, mutableSetOf(c))
                        }
                    }
                }
            }
            transitions[state.id] = stateTransitions.toMap()
        }

        // Saving transition table
        out.print(transitions.size)
        for ((state, t) in transitions) {
            out.print(state)
            out.print(t.size)
            for ((id, chars) in t) {
                out.print(id)
                out.print(chars.size)
                chars.forEach(out::print)
            }
        }

        // Saving state info
        out.print(states.size)
        for (state in states) {
            // Save state id
            out.print(state.id)

            // If state is acceptable write token name and tell skipped or not
            if (state.accept) {
                out.print(1.toByte())
                val token = state.tokens.first()
                if (token in skippedTokens) {
                    out.print(1.toByte())
                } else {
                    out.print(0.toByte())
                }
                out.print(token)
            } else {
                out.print(0.toByte())
            }
        }
    }


    companion object {
        /**
         * ε-closure
         */
        private fun epsilonClosure(states: Set<State>): Set<State> {
            val result = states.toMutableSet()
            val queue = states.toMutableList()

            while (queue.isNotEmpty()) {
                val s = queue.first()
                queue.removeAt(0)

                for (transition in s.getTransitions(Epsilon)) {
                    if (transition !in result) {
                        // If transition state not seen
                        result.add(transition)
                        queue.add(transition)
                    }
                }
            }

            return result
        }

        /**
         * NFA move
         * @param states From
         * @param char Transition char
         * @return States after move from @states by @char
         */
        private fun move(states: Set<State>, char: Char): Set<State> {
            val result = mutableSetOf<State>()
            for (s in states) {
                for (t in s.getTransitions(char)) {
                    result.add(t)
                }
            }
            return result.toSet()
        }

        /**
         * Minimizing DFA
         * @param alphabet ∑
         * @param begin Begin state of NFA
         * @param finals Acceptable states
         * @param oldStates DFA states
         */
        private fun minimize(alphabet: Set<Char>, begin: State, finals: Set<State>, oldStates: Set<State>): DFA {
            val minIdCounter = IDCounter()

            // Key: States
            // Val: Chars
            val table = mutableMapOf<MutableSet<State>, MutableSet<Char>>()

            // Finals
            val fBlock = mutableSetOf<State>()
            // Not finals
            val bBlock = mutableSetOf<State>()

            // Refine states by acceptable and not
            for (state in oldStates) {
                if (state.accept) {
                    fBlock.add(state)
                } else {
                    bBlock.add(state)
                }
            }

            val blocks = mutableSetOf<MutableSet<State>>()

            // Get minimum block between finals blocks and not
            var minBlock = if (fBlock.size != oldStates.size && fBlock.isNotEmpty()) {
                val f = fBlock.toMutableSet()
                val b = bBlock.toMutableSet()
                blocks.add(f)
                blocks.add(b)
                if (f.size < b.size) {
                    f
                } else {
                    b
                }
            } else {
                oldStates.toMutableSet()
            }

            // Add all alphabet to minimum table
            table[minBlock] = alphabet.toMutableSet()

            while (table.isNotEmpty()) {
                val entry = table.entries.first()
                val block = entry.key
                val chars = entry.value
                val c = chars.first()
                chars.remove(c)
                if (chars.isEmpty() && table.containsKey(block)) {
                    // Remove if char's set is empty
                    table.remove(block)
                }

                val newBlocks = mutableSetOf<MutableSet<State>>()
                for (blocksItem in blocks) {
                    bBlock.clear()
                    fBlock.clear()
                    for (blockItem in blocksItem) {
                        val transitions = blockItem.getTransitions(c)
                        if (transitions.isNotEmpty()) {
                            if (block.contains(transitions.first())) {
                                fBlock.add(blockItem)
                            } else {
                                bBlock.add(blockItem)
                            }
                        } else {
                            bBlock.add(blockItem)
                        }
                    }

                    if (bBlock.size != blocksItem.size && bBlock.isNotEmpty()) {
                        val f = fBlock.toMutableSet()
                        val b = bBlock.toMutableSet()
                        newBlocks.add(f)
                        newBlocks.add(b)
                        minBlock = if (f.size < b.size) {
                            f
                        } else {
                            b
                        }

                        for (char in alphabet) {
                            when {
                                table[blocksItem]?.contains(c) == true -> {
                                    table[blocksItem]!!.remove(c)
                                    if (table[blocksItem]!!.isEmpty()) {
                                        table.remove(blocksItem)
                                    }
                                    table[f] = mutableSetOf(char)
                                    table[b] = mutableSetOf(char)
                                }
                                table.containsKey(minBlock) -> table[minBlock]!!.add(char)
                                else -> table[minBlock] = mutableSetOf(char)
                            }
                        }
                    } else {
                        newBlocks.add(blocksItem)
                    }
                }
                blocks.clear()
                blocks.addAll(newBlocks)
            }

            val endBlocks = mutableSetOf<MutableSet<State>>()
            var beginBlock: MutableSet<State>? = null
            for (block in blocks) {
                if (block.contains(begin)) {
                    beginBlock = block
                }
                if (finals.contains(block.first())) {
                    endBlocks.add(block)
                }
            }

            val newStates = mutableSetOf<State>()
            var states = merge(beginBlock!!, minIdCounter)
            newStates.addAll(states)
            blocks.remove(beginBlock)

            for (block in blocks) {
                states = merge(block, minIdCounter)
                newStates.addAll(states)
            }

            return DFA(
                alphabet,
                newStates
            )
        }

        /**
         * Merging block
         * @param block States for merging
         * @param minIdCounter IDCounter
         * @return Merged states
         */
        private fun merge(block: MutableSet<State>, minIdCounter: IDCounter): MutableSet<State> {
            val result = mutableSetOf<State>()
            val myBlock = mutableSetOf<State>()
            val delStates = mutableSetOf<State>()

            myBlock.addAll(block)

            while (myBlock.isNotEmpty()) {
                delStates.clear()
                val newState = State(minIdCounter.next)
                for (blockItem in myBlock) {
                    var skip = false
                    if (blockItem.accept) {
                        if (newState.accept) {
                            if (blockItem.tokens.size != newState.tokens.size) {
                                skip = true
                            } else {
                                for (token in blockItem.tokens) {
                                    var has = false
                                    for (t in newState.tokens) {
                                        if (t == token) {
                                            has = true
                                        }
                                    }
                                    if (!has) {
                                        skip = true
                                    }

                                }
                            }
                        } else {
                            newState.accept = true
                            newState.addTokens(blockItem.tokens)
                        }
                    }

                    if (!skip) {
                        delStates.add(blockItem)
                        for ((c, states) in blockItem.getAllTransitions()) {
                            for (state in states) {
                                state.removeInputState(c, blockItem)
                                newState.transition(c, state)
                            }
                        }

                        for ((c, states) in blockItem.getAllInputStates()) {
                            for (state in states) {
                                state.removeTransition(c, blockItem)
                                state.transition(c, newState)
                            }
                        }
                    }
                }

                myBlock.removeAll(delStates)
                result.add(newState)
            }

            return result
        }

        /**
         * @return Set of token names
         */
        private fun getTokens(states: Set<State>): Set<String> {
            return sequence {
                states.forEach {
                    yieldAll(it.tokens)
                }
            }.toSet()
        }

        /**
         * Create DFA by NFA
         * @param nfa NFA
         * @param alphabet ∑
         * @return DFA
         */
        fun ofNFA(nfa: NFA, alphabet: Set<Char>): DFA {
            val idCounter = IDCounter()

            val table = mutableMapOf<Set<State>, State>()
            val queue = mutableListOf<Set<State>>()
            val finals = mutableSetOf<State>()

            var states = epsilonClosure(mutableSetOf(nfa.begin))
            val newBegin = State(idCounter.next)
            var tokens = getTokens(states)
            if (tokens.isNotEmpty()) {
                newBegin.accept = true
                newBegin.addTokens(tokens)
                finals.add(newBegin)
            }

            table[states] = newBegin

            queue.add(states)

            while (queue.isNotEmpty()) {
                states = queue.first()
                queue.removeAt(0)
                val state = table[states]!!
                for (c in alphabet) {
                    val m = move(states, c)
                    if (m.isNotEmpty()) {
                        val newStates = epsilonClosure(m)
                        if (table.containsKey(newStates)) {
                            state.transition(c, table[newStates]!!)
                        } else {
                            val newState = State(idCounter.next)
                            tokens = getTokens(newStates)
                            if (tokens.isNotEmpty()) {
                                newState.accept = true
                                newState.addTokens(tokens)
                                finals.add(newState)
                            }
                            table[newStates] = newState
                            state.transition(c, newState)
                            queue.add(newStates)
                        }
                    }
                }
            }

            return minimize(alphabet, newBegin, finals.toSet(), table.map { it.value }.toSet())
        }
    }

}
