package ru.egor9814.spb.builder.fsm

/**
 * Finite machine state
 * @param id Id of state
 * @param accept Acceptable flag
 */
class State(
    val id: Int,
    var accept: Boolean = false
) {

    private val outputStates = mutableMapOf<Character, MutableSet<State>>()
    private val inputStates = mutableMapOf<Character, MutableSet<State>>()

    private val mTokens = mutableSetOf<String>()

    /**
     * Add transition from current state to other state by Character
     */
    fun transition(c: Character, state: State) {
        outputStates[c]?.add(state) ?: outputStates.put(c, mutableSetOf(state))
        state.inputStates.let { it[c]?.add(this) ?: it.put(c, mutableSetOf(this)) }
    }

    /**
     * Add transition from current state to other state by Usual char
     */
    fun transition(c: Char, state: State) {
        transition(Character.Normal(c), state)
    }

    /**
     * Add transition from current state to other states by Character
     */
    fun transition(c: Character, states: Set<State>) {
        states.forEach {
            transition(c, it)
        }
    }

    /*fun transition(c: Char, states: Set<State>) {
        states.forEach {
            transition(Character.Normal(c), it)
        }
    }

    fun hasTransitions(c: Character) = outputStates[c]?.isNotEmpty() ?: false*/

    /**
     * Get transitions by Character
     */
    fun getTransitions(c: Character) = outputStates[c]?.toSet() ?: emptySet()

    /**
     * Get transitions by Usual char
     */
    fun getTransitions(c: Char) = outputStates[Character.Normal(c)]?.toSet() ?: emptySet()

    /**
     * Get all transitions
     */
    fun getAllTransitions() = outputStates.map { (k, v) -> k to v.toSet() }.toMap()

    /**
     * Remove state from current state by Character
     */
    fun removeTransition(c: Character, state: State) {
        outputStates[c]?.remove(state)
    }


    /*fun hasInputStates(c: Character) = inputStates[c]?.isNotEmpty() ?: false

    fun getInputStates(c: Character) = inputStates[c]?.toSet() ?: emptySet()*/

    /**
     * Get all input states
     */
    fun getAllInputStates() = inputStates.map { (k, v) -> k to v.toSet() }.toMap()

    /**
     * Remove input state from current state by Character
     */
    fun removeInputState(c: Character, state: State) {
        inputStates[c]?.remove(state)
    }


    /**
     * Add set of token names to current state
     */
    fun addTokens(tokens: Set<String>) {
        mTokens.addAll(tokens)
    }

    /**
     * Add token name to current state
     */
    fun addToken(token: String) {
        mTokens.add(token)
    }

    /**
     * Get all tokens of current states
     */
    val tokens get() = mTokens.toSet()


    override fun toString(): String {
        val res = "($id)"
        return if (accept) "($res)" else res
    }

    override fun equals(other: Any?): Boolean {
        return other is State && id == other.id && accept == other.accept
    }

    override fun hashCode(): Int {
        return 31 * (31 * id.hashCode() + accept.hashCode())
    }

}
