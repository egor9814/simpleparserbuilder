package ru.egor9814.spb.builder.fsm

/**
 * Class for representation transition
 * */
sealed class Character {
    /**
     * Usual-move
     * */
    class Normal(val c: Char) : Character() {
        override fun toString(): String {
            return "$c"
        }

        override fun equals(other: Any?): Boolean {
            return other is Normal && other.c == c
        }

        override fun hashCode(): Int {
            return c.hashCode()
        }
    }
}

/**
 * ε-move
 * */
object Epsilon : Character() {
    override fun toString(): String {
        return "ε"
    }

    override fun equals(other: Any?): Boolean {
        return other is Epsilon
    }

    override fun hashCode(): Int {
        return 31 * 'ε'.hashCode()
    }
}
