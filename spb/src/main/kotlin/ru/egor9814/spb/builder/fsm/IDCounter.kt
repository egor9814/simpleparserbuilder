package ru.egor9814.spb.builder.fsm

/**
 * Special class for calculate states IDs
 */
class IDCounter {

    private var id = 0

    val next get() = id++

}
