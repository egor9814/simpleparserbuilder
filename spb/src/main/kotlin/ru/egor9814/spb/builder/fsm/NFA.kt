package ru.egor9814.spb.builder.fsm

/**
 * Non deterministic finite automata
 * @param begin Begin state
 * @param final Final state
 */
data class NFA(
    val begin: State,
    val final: State
) {

    companion object {
        /**
         * Make NFA of char: (A) -- char --> (B)
         */
        fun ofChar(char: Char, id: IDCounter): NFA {
            val begin = State(id.next)
            val final = State(id.next, true)
            begin.transition(char, final)
            return NFA(begin, final)
        }

        /**
         * Make NFA of union:
         * (A) -- ε --> a -- ε --> (B)
         *    \-- ε --> b -- ε -->/
         */
        fun ofUnion(a: NFA, b: NFA, id: IDCounter): NFA {
            val begin = State(id.next)
            val final = State(id.next, true)

            begin.transition(Epsilon, a.begin)
            begin.transition(Epsilon, b.begin)

            a.final.apply {
                accept = false
                transition(Epsilon, final)
            }
            b.final.apply {
                accept = false
                transition(Epsilon, final)
            }
            return NFA(begin, final)
        }

        /**
         * Make NFA of concat:
         * a -- ε --> b
         */
        fun ofConcat(a: NFA, b: NFA): NFA {
            a.final.apply {
                accept = false
                transition(Epsilon, b.begin)
            }
            return NFA(a.begin, b.final)
        }

        /**
         * Make NFA of Kleene Closure (zero-or-more):
         * A = nfa.begin
         * B = nfa.final
         * (C) -- ε --> A ........ B -- ε --> (D)
         *   \           \<-- ε --/          /
         *   \------------------------ ε -->/
         */
        fun ofKleeneClosure(nfa: NFA, id: IDCounter): NFA {
            val begin = State(id.next)
            val final = State(id.next, true)

            begin.transition(Epsilon, setOf(final, nfa.begin))
            nfa.final.apply {
                accept = false
                transition(Epsilon, final)
                transition(Epsilon, nfa.begin)
            }

            return NFA(begin, final)
        }


        /**
         * Make NFA of one-or-more:
         * A = nfa.begin
         * B = nfa.final
         * (C) -- ε --> A ........ B -- ε --> (D)
         *               \<-- ε --/
         */
        fun ofOneOrMore(nfa: NFA, id: IDCounter): NFA {
            val begin = State(id.next)
            val final = State(id.next, true)

            begin.transition(Epsilon, nfa.begin)
            nfa.final.apply {
                accept = false
                transition(Epsilon, final)
                transition(Epsilon, nfa.begin)
            }

            return NFA(begin, final)
        }


        /**
         * Make NFA of Optional (zero-or-one):
         * A = nfa.begin
         * B = nfa.final
         * (C) -- ε --> A ........ B -- ε --> (D)
         *   \                               /
         *   \------------------------ ε -->/
         */
        fun ofZeroOrOne(nfa: NFA, id: IDCounter): NFA {
            val begin = State(id.next)
            val final = State(id.next, true)

            begin.transition(Epsilon, setOf(final, nfa.begin))
            nfa.final.apply {
                accept = false
                transition(Epsilon, final)
            }

            return NFA(begin, final)
        }

        /**
         * Make final NFA
         * @param nfas Table of pair (Token name, NFA)
         */
        fun makeFinal(nfas: Map<String, NFA>, id: IDCounter): NFA {
            val begin = State(id.next)
            val final = State(id.next, true)

            for ((token, nfa) in nfas) {
                begin.transition(Epsilon, nfa.begin)
                nfa.final.apply {
                    addToken(token)
                    transition(Epsilon, final)
                }
            }

            return NFA(begin, final)
        }
    }

}
