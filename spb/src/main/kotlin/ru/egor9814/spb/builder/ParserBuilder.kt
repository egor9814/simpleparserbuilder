package ru.egor9814.spb.builder

import ru.egor9814.spb.builder.fsm.DFA
import ru.egor9814.spb.builder.fsm.DFAWriter
import ru.egor9814.spb.design.LangConfiguration
import java.io.File
import java.io.IOException
import java.io.PrintStream
import kotlin.math.roundToInt

/**
 * Builder class for TokenType, Lexer and Parser
 * @param conf Configuration of language
 */
class ParserBuilder(val conf: LangConfiguration) {

    private fun log(msg: String) {
        print(msg)
    }

    private fun logln(msg: String) {
        println(msg)
    }

    /**
     * Build and to @outDir folder
     * @param outDir Output folder for sources
     */
    fun build(outDir: File) {
        logln("getting output source dir")
        val out = conf.packageName.let {
            when {
                it.isEmpty() -> outDir
                it.contains('.') -> File(outDir, it.split('.').joinToString(separator = "/"))
                else -> File(outDir, it)
            }
        }
        if (!out.exists()) {
            if (!out.mkdirs())
                throw IOException("cannot create output source dir")
        }
        logln("output source dir: ${out.absolutePath}")

        logln("getting output parser file")
        val parser = File(out, "${conf.langName}Parser.kt")
        if (!parser.exists()) {
            if (!parser.createNewFile())
                throw IOException("cannot create parser file")
        }
        logln("output parser file: ${parser.absolutePath}")

        logln("getting output lexer file")
        val lexer = File(out, "${conf.langName}Lexer.kt")
        if (!lexer.exists()) {
            if (!lexer.createNewFile())
                throw IOException("cannot create lexer file")
        }
        logln("output lexer file: ${lexer.absolutePath}")

        logln("getting output token types file")
        val tokenTypes = File(out, "${conf.langName}TokenType.kt")
        if (!tokenTypes.exists()) {
            if (!tokenTypes.createNewFile())
                throw IOException("cannot create token types file")
        }
        logln("output token types file: ${tokenTypes.absolutePath}")

        tokenTypes.outputStream().use {
            PrintStream(it).apply {
                logln("generating token types...")
                println("/* Auto-Generated Recursive Descent Parser by " +
                        "${conf.author.let { name -> if (name.size == 1) name[0] else name.joinToString() }} */")

                if (conf.packageName.isNotEmpty()) {
                    println("\npackage ${conf.packageName}")
                }

                println("\nimport ru.egor9814.spb.lib.lexer.*")

                println("\nobject ${tokenTypes.nameWithoutExtension} : TokenTypeProvider() {")

                logln("  resolving tokens...")
                val tokens = conf.lexer.getAll().keys.toMutableList()
                require("EOF" !in tokens)
                require("ERROR" !in tokens)
                tokens.add(0, "ERROR")
                tokens.add(0, "EOF")

                val maxLength = sequence {
                    yieldAll(tokens)
                }.map { t -> t.length }.max()!!

                logln("  generating token types...")
                if (tokens.isNotEmpty()) {
                    val step = 100.0 / tokens.size
                    var pct = step
                    for ((i, t) in tokens.withIndex()) {
                        log("    generating token type \"$t\"...")
                        print("    val $t = ")
                        print(sequence { while (true) yield(' ') }.take(maxLength - t.length).joinToString(""))
                        println("object : SimpleTokenType(\"$t\", $i) {}")
                        logln(" (${pct.roundToInt()}%)")
                        pct += step
                    }

                    logln("  generating token type registration...")
                    println("    init {")
                    for (t in tokens) {
                        println("        register($t)")
                    }
                    println("    }")
                }
                println("}")
                logln("generating token types done!")
            }
        }

        lexer.outputStream().use {
            DFAWriter(PrintStream(it), conf).apply {
                logln("generating lexer...")
                header()

                val l = conf.lexer

                logln("  generating NFA...")
                val nfa = l.getNFA()

                logln("  resolving Alphabet...")
                val alphabet = l.getAlphabet()

                logln("  resolving skipped tokens...")
                val skipped = l.getAll().filter { (_, v) -> v }.map { (k, _) -> k }.toSet()

                logln("  generating DFA...")
                val dfa = DFA.ofNFA(nfa, alphabet)

                logln("  generating transition table...")
                write(dfa, skipped)

                logln("  finalizing...")
                footer()

                logln("generaing lexer done!")
            }
        }

        parser.outputStream().use {
            PrintStream(it).apply {
                logln("generating parser...")
                println("/* Auto-Generated Recursive Descent Parser by " +
                        "${conf.author.let { name -> if (name.size == 1) name[0] else name.joinToString() }} */")

                if (conf.packageName.isNotEmpty()) {
                    println("\npackage ${conf.packageName}")
                }

                println("\nimport ru.egor9814.spb.lib.lexer.*")
                println("\nimport ru.egor9814.spb.lib.parser.*")

                val tt = tokenTypes.nameWithoutExtension
                println("\nclass ${parser.nameWithoutExtension}(tokens: List<Token>) : ParserBase<$tt>(tokens, $tt) {")

                logln("  generating rules...")
                SourceWriter(this).apply {
                    var pct: Double
                    conf.rules.getAll().apply {
                        val step = 100.0 / (size + 2)
                        pct = step
                        forEach { (name, s) ->
                            log("    generating rule '$name'...")
                            val start = System.currentTimeMillis()
                            func("r$name", synthetic = false, bodyIsTerminal = s.isTerminal, body = s::generateCode)

                            val elapses = System.currentTimeMillis() - start
                            logln(" (${elapses}ms, ${pct.roundToInt()}%)")
                            pct += step
                        }
                    }
                    log("    generating sub rules...")
                    var start = System.currentTimeMillis()
                    doLater()
                    var elapses = System.currentTimeMillis() - start
                    logln(" (${elapses}ms, ${pct.roundToInt()}%)")

                    log("    generating main rule call...")
                    start = System.currentTimeMillis()
                    println("override fun main(result: Node): Boolean {", 1)
                    println("return r${conf.mainRule}(result)", 2)
                    println("}", 1)
                    elapses = System.currentTimeMillis() - start
                    logln(" (${elapses}ms, 100%)")
                }
                logln("  generating rules done!")

                println("}")
                logln("generating parser done!")
            }
        }

        logln("done!")
    }

}
