package ru.egor9814.spb.builder.fsm

import ru.egor9814.spb.design.LangConfiguration
import java.io.PrintStream

/**
 * Special writer for generating Lexer
 * @param out Lexer file output stream
 * @param conf Configuration of language
 */
class DFAWriter(
    private val out: PrintStream,
    private val conf: LangConfiguration
) {

    /**
     * Print header to lexer: includes author, package name
     */
    fun header() {
        out.apply {
            println("/* Auto-Generated Recursive Descent Parser by " +
                    "${conf.author.let { name -> if (name.size == 1) name[0] else name.joinToString() }} */")

            if (conf.packageName.isNotEmpty()) {
                println("\npackage ${conf.packageName}\n")
            }

            println("import ru.egor9814.spb.lib.lexer.*\n")

            print("class ${conf.langName}Lexer(source: SourceCode) : LexerBase(source, ${conf.langName}TokenType")
        }
    }

    // Bytes count in a row
    private var count = 0

    /**
     * Print byte in DFA bytes
     */
    internal fun print(byte: Byte) {
        if (count == 0) {
            out.println(",")
            out.print("    ")
        } else {
            out.print(", ")
        }
        out.print("$byte.toByte()")
        if (++count == 8) {
            count = 0
        }
    }

    /**
     * Print footer to lexer
     */
    fun footer() {
        out.apply {
            println(")")
        }
    }


    /**
     * Save DFA to lexer
     * @param dfa DFA
     * @param skippedTokens Set of skipped token names
     */
    fun write(dfa: DFA, skippedTokens: Set<String>) {
        dfa.write(this, skippedTokens)
    }


    /**
     * Print bytes to DFA bytes
     */
    internal fun print(bytes: ByteArray) {
        bytes.forEach(this::print)
    }

    /**
     * Convert int to bytes and print
     */
    internal fun print(int: Int) {
        print(Array(4) { (int shr (8 * it) and 0xff).toByte() }.toByteArray())
    }

    /**
     * Convert short to bytes and print
     */
    internal fun print(short: Short) {
        val int = short.toInt()
        print(Array(2) { (int shr (8 * it) and 0xff).toByte() }.toByteArray())
    }

    /**
     * Convert char to bytes and print
     */
    internal fun print(char: Char) {
        print(char.toShort())
    }

    /**
     * Convert string to bytes and print
     */
    internal fun print(string: String) {
        print(string.length)
        string.forEach(this::print)
    }

}
