package ru.egor9814.spb.design

/**
 * Language configuration file
 */
abstract class LangConfiguration {

    /**
     * Language package name
     */
    var packageName = ""
        protected set

    /**
     * Language name
     */
    var langName = ""
        protected set

    /**
     * Authors or author of Language
     */
    var author = emptyArray<String>()
        protected set

    private lateinit var mParser: ParserConfiguration<*>
    private lateinit var mMain: String
    private lateinit var mLexer: LexerConfiguration

    /**
     * Set parser configuration, and set main rule name
     */
    protected fun setParserRules(parserRules: ParserConfiguration<*>, mainRule: () -> String) {
        mParser = parserRules
        mMain = mainRule()
        mLexer = parserRules.l
    }

    /**
     * Parser configuration file
     */
    val rules: ParserConfiguration<*>
        get() = mParser

    /**
     * Main rule name
     */
    val mainRule: String
        get() = mMain

    /**
     * Lexer configuration file
     */
    val lexer: LexerConfiguration
        get() = mLexer

}
