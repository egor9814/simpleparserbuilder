package ru.egor9814.spb.design

import ru.egor9814.spb.builder.SourceWriter
import kotlin.reflect.KProperty

/**
 * Parser configuration file
 * @param l Lexer configuration file
 */
abstract class ParserConfiguration<out L : LexerConfiguration>(
    val l: L
) {

    private abstract class NonTerminal : RuleSymbol {
        override val isTerminal = false
    }

    private class TokenLink(val token: LexerConfiguration.TokenRule) : RuleSymbol {
        override val isTerminal = true

        override fun toString(): String {
            return "[${token.name}]"
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("check(TokenType.${token.name}, rn)")
            }
        }
    }

    /**
     * Get token from Lexer rules
     */
    protected fun T(token: L.() -> LexerConfiguration.TokenRule): RuleSymbol {
        return TokenLink(token(l))
    }

    private class Alternatives : NonTerminal() {
        val symbols = mutableListOf<RuleSymbol>()

        override fun toString(): String {
            return symbols.joinToString(prefix = "<", postfix = ">", separator = " | ") {
                "($it)"
            }
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("if (r) while (true) {")
                indent {
                    println("save()")
                    symbols.withIndex().forEach { (i, s) ->
                        if (i != 0) {
                            print("if (r) { popState(); break } else ")
                        }
                        println("r = checked(\"Alternative read tokens, but did not complete\") {",
                            if (i == 0) currentIndentLevel else 0)
                        if (s.isTerminal) {
                            s.generateCode(currentIndentLevel + 1, "$currentRuleName$i", out)
                        } else {
                            println("$currentRuleName$i(rn)", currentIndentLevel + 1)
                            doLater {
                                func("$currentRuleName$i", body = s::generateCode)
                            }
                        }
                        println("}")
                    }
                    println("if (!r) {")
                    indent {
                        println("check(\"Alternative not selected, " +
                                "but tokens have already been read in one of them\")")
                    }
                    println("}")
                    println("popState()")
                    println("break")
                }
                println("} else return false")
            }
        }
    }

    /**
     * Rules alternatives
     */
    protected infix fun RuleSymbol.or(other: RuleSymbol): RuleSymbol {
        return if (this is Alternatives) {
            this.apply {
                symbols.add(other)
            }
        } else {
            Alternatives().apply {
                symbols.add(this@or)
                symbols.add(other)
            }
        }
    }


    private class Sequence : NonTerminal() {
        val symbols = mutableListOf<RuleSymbol>()

        override fun toString(): String {
            return symbols.joinToString(prefix = "<", postfix = ">", separator = " ") {
                "($it)"
            }
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.apply {
                symbols.withIndex().forEach { (i, s) ->
                    if (s.isTerminal) {
                        out.withIndent(indentLevel) {
                            print("if (!r) return false else r = ")
                        }
                        s.generateCode(0, "$currentRuleName$i", out)
                    } else {
                        s.generateCode(indentLevel, "$currentRuleName$i", out)
                    }
                }
            }
        }
    }

    /**
     * Rules sequence
     */
    protected operator fun RuleSymbol.rangeTo(other: RuleSymbol): RuleSymbol {
        return if (this is Sequence) {
            this.apply {
                symbols.add(other)
            }
        } else {
            Sequence().apply {
                symbols.add(this@rangeTo)
                symbols.add(other)
            }
        }
    }


    private class ZeroOrOne(val symbol: RuleSymbol) : NonTerminal() {
        override fun toString(): String {
            return "{$symbol}?"
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("if (r) {")
                indent {
                    print("r = ")
                }
                if (symbol.isTerminal) {
                    symbol.generateCode(0, "${currentRuleName}ZoO", out)
                } else {
                    println("${currentRuleName}ZoO(rn)", 0)
                    doLater {
                        func("${currentRuleName}ZoO", body = symbol::generateCode)
                    }
                }
                println("} else return false")
                println("r = true")
            }
        }
    }

    /**
     * Rule?
     */
    protected val RuleSymbol.zeroOrOne: RuleSymbol
        get() = ZeroOrOne(this)


    private class ZeroOrMore(val symbol: RuleSymbol) : NonTerminal() {
        override fun toString(): String {
            return "{$symbol}*"
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("if (r) do {")
                indent {
                    print("r = ")
                }
                if (symbol.isTerminal) {
                    symbol.generateCode(0, "${currentRuleName}ZoM", out)
                } else {
                    println("${currentRuleName}ZoM(rn)", 0)
                    doLater {
                        func("${currentRuleName}ZoM", body = symbol::generateCode)
                    }
                }
                println("} while (r) else return false")
                println("r = true")
            }
        }
    }

    /**
     * Rule*
     */
    protected val RuleSymbol.zeroOrMore: RuleSymbol
        get() = ZeroOrMore(this)


    private class OneOrMore(val symbol: RuleSymbol) : NonTerminal() {
        override fun toString(): String {
            return "{$symbol}+"
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("if (!r) return false")
                print("r = ")
                if (symbol.isTerminal) {
                    symbol.generateCode(0, "${currentRuleName}OoM", out)
                } else {
                    println("${currentRuleName}OoM(rn)", 0)
                    doLater {
                        func("${currentRuleName}OoM", body = symbol::generateCode)
                    }
                }
                println("if (r) do {")
                indent {
                    print("r = ")
                }
                if (symbol.isTerminal) {
                    symbol.generateCode(0, "${currentRuleName}OoM", out)
                } else {
                    println("${currentRuleName}OoM(rn)", 0)
                }
                println("} while (r) else return false")
                println("r = true")
            }
        }
    }

    /**
     * Rule+
     */
    protected val RuleSymbol.oneOrMore: RuleSymbol
        get() = OneOrMore(this)


    protected object N {
        sealed class CountNumber {
            object NCount : CountNumber() {
                override fun toString() = "n"
            }
            object ZeroCount : CountNumber() {
                override fun toString() = "0"
            }
            object OneCount : CountNumber() {
                override fun toString() = "1"
            }
        }
        data class Count(
            val first: CountNumber,
            val last: CountNumber
        )

        val n = CountNumber.NCount
        val zero = CountNumber.ZeroCount
        val one = CountNumber.OneCount

        operator fun CountNumber.rangeTo(end: CountNumber): Count {
            return Count(this, end)
        }

        operator fun Int.rangeTo(end: CountNumber): Count {
            return when (this) {
                0 -> zero..end
                1 -> one..end
                else -> throw RuntimeException("invalid range ($this .. $end)")
            }
        }

        operator fun get(symbol: RuleSymbol): (N.() -> Count) -> RuleSymbol {
            return { count ->
                val c = count(this)
                getSymbol(c, symbol)
            }
        }

        private fun getSymbol(c: Count, symbol: RuleSymbol): RuleSymbol {
            if (c.first === zero) {
                if (c.last === n) {
                    return ZeroOrMore(symbol)
                } else if (c.last === one) {
                    return ZeroOrOne(symbol)
                }
            } else if (c.first === one) {
                if (c.last === n) {
                    return OneOrMore(symbol)
                }
            }
            throw RuntimeException("invalid N range: $c")
        }
    }


    private open class RuleLink internal constructor(val rule: String, val callPrefix: String = "r") : RuleSymbol {
        override val isTerminal = true

        override fun toString(): String {
            return "[$callPrefix$rule]"
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            out.withIndent(indentLevel) {
                println("$callPrefix$rule(rn)")
            }
        }
    }

    /**
     * Rule link
     */
    protected fun R(rule: () -> String): RuleSymbol {
        return Sequence().apply {
            symbols.add(RuleLink(rule()))
        }
    }


    private val rules = mutableMapOf<String, RuleSymbol>()
    fun getAll(): Map<String, RuleSymbol> {
        return rules.toMap()
    }

    /**
     * Register rule
     */
    protected fun rule(name: String, symbol: () -> RuleSymbol) {
        rules[name] = symbol()
    }


    protected class Fragment(
        private val symbol: RuleSymbol
    ) {
        operator fun getValue(thisRef: Any?, property: KProperty<*>): RuleSymbol {
            return symbol
        }
    }
    private class RuleSymbolHolder(symbol: () -> RuleSymbol) : NonTerminal() {
        private val symbol by lazy(symbol::invoke)
        override fun toString(): String {
            return symbol.toString()
        }

        override fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter) {
            symbol.generateCode(indentLevel, currentRuleName, out)
        }
    }

    /**
     * Rule fragment
     */
    protected fun fragment(symbol: () -> RuleSymbol): Fragment {
        return Fragment(RuleSymbolHolder(symbol))
    }

}
