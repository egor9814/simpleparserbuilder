package ru.egor9814.spb.design

import ru.egor9814.spb.builder.SourceWriter

interface RuleSymbol {

    val isTerminal: Boolean

    fun generateCode(indentLevel: Int, currentRuleName: String, out: SourceWriter)

}
