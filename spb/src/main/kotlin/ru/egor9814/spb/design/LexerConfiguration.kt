package ru.egor9814.spb.design

import ru.egor9814.spb.builder.fsm.IDCounter
import ru.egor9814.spb.builder.fsm.NFA
import kotlin.reflect.KProperty

/**
 * Lexer configuration file
 */
abstract class LexerConfiguration {

    companion object {
        /**
         * Convert char to Unicode value
         */
        internal fun wrapEscape(char: Char) = char.let {
            var code = it.toInt().toString(16).toUpperCase()
            while (code.length < 4)
                code = "0$code"
            "\\u$code"
        }
    }

    private val tokens = mutableMapOf<String, TokenRule>()

    /**
     * Get all pairs (Token name, Skipped)
     */
    fun getAll() = tokens.map { (k, v) -> k to v.skip }.toMap()

    /**
     * Generate NFA
     */
    fun getNFA(): NFA {
        val nfas = mutableMapOf<String, NFA>()
        val idCounter = IDCounter()
        for ((token, rule) in tokens) {
            nfas[token] = rule.symbol.toNFA(idCounter)
        }
        return NFA.makeFinal(nfas, idCounter)
    }

    /**
     * Get ∑ of NFA
     */
    fun getAlphabet(): Set<Char> {
        val res = mutableSetOf<Char>()
        for ((_, r) in tokens)
            r.symbol.getAlphabet(res)
        return res.toSet()
    }


    /**
     * Token rule
     * @param name Token name
     * @param symbol Rule for creation NFA
     */
    data class TokenRule(
        val name: String,
        val symbol: TokenSymbol,
        private val l: LexerConfiguration
    ) {
        /**
         * Skip or not this token in Lexical analysis
         */
        var skip = false

        init {
            if (!l.tokens.containsKey(name))
                l.tokens[name] = this
        }
    }

    /**
     * Special delegate for get TokenRule
     * @param l This configuration
     * @param symbol NFA's creation rule
     */
    protected class TokenDelegate(
        private val l: LexerConfiguration,
        symbol: () -> TokenSymbol
    ) {
        private val symbol by lazy { symbol() }

        operator fun getValue(thisRef: Any?, property: KProperty<*>): TokenRule {
            val name = property.name.capitalize()
            return l.tokens[name] ?: TokenRule(name, symbol, l)
        }
    }

    /**
     * Register token as concat chars in 'value'
     */
    protected fun token(value: String) = TokenDelegate(this) { T(value) }

    /**
     * Register token
     */
    protected fun token(symbol: () -> TokenSymbol) = TokenDelegate(this, symbol)


    /**
     * Terminal char
     */
    private class Terminal(val char: Char) : TokenSymbol {
        override fun toString(): String {
            return "'${wrapEscape(char)}'"
        }

        override fun toNFA(idCounter: IDCounter): NFA {
            return NFA.ofChar(char, idCounter)
        }

        override fun getAlphabet(out: MutableSet<Char>) {
            out.add(char)
        }
    }

    protected object T {
        /**
         * One terminal char
         */
        operator fun get(value: Char): TokenSymbol {
            return Terminal(value)
        }

        /**
         * Union of chars: `ab` -> `a | b`, `0-9` -> `0 | 1 | ... | 9`
         */
        operator fun get(value: String): TokenSymbol {
            require(value.isNotEmpty()) {
                "String must contain one char or more"
            }
            val chars = mutableSetOf<Char>()
            var i = 0
            while (i < value.length) {
                if (i > 0 && value[i] == '-' && i + 1 < value.length) {
                    if (value[i + 1] == '-') {
                        chars.add('-')
                        i++
                    } else {
                        for (c in (value[i-1] + 1)..(value[++i])) {
                            chars.add(c)
                        }
                    }
                } else {
                    chars.add(value[i])
                }
                i++
            }
            var res: TokenSymbol = Terminal(chars.first())
            for (c in chars.drop(1)) {
                res = Alternatives(res, Terminal(c))
            }
            return res
        }

        /**
         * Concat of chars
         */
        operator fun invoke(value: String): TokenSymbol {
            require(value.isNotEmpty()) {
                "String must contain one char or more"
            }
            var res: TokenSymbol = Terminal(value.first())
            for (c in value.drop(1)) {
                res = Sequence(res, Terminal(c))
            }
            return res
        }

        /**
         * Get symbol from rule
         */
        operator fun invoke(token: () -> TokenRule): TokenSymbol {
            return token().symbol
        }
    }



    protected class Fragment(
        symbol: () -> TokenSymbol
    ) {
        private val symbol by lazy { symbol() }

        operator fun getValue(thisRef: Any?, property: KProperty<*>): TokenSymbol {
            return symbol
        }
    }

    /**
     * Make fragment of rule
     */
    protected fun fragment(symbol: () -> TokenSymbol): Fragment {
        return Fragment(symbol)
    }


    private class Alternatives(val left: TokenSymbol, val right: TokenSymbol) : TokenSymbol {
        override fun toString(): String {
            return "< $left | $right >"
        }

        override fun toNFA(idCounter: IDCounter): NFA {
            return NFA.ofUnion(left.toNFA(idCounter), right.toNFA(idCounter), idCounter)
        }

        override fun getAlphabet(out: MutableSet<Char>) {
            left.getAlphabet(out)
            right.getAlphabet(out)
        }
    }

    /**
     * Union
     */
    protected infix fun TokenSymbol.or(other: TokenSymbol): TokenSymbol {
        return Alternatives(this, other)
    }


    private class Sequence(val left: TokenSymbol, val right: TokenSymbol) : TokenSymbol {
        override fun toString(): String {
            return "( $left $right )"
        }

        override fun toNFA(idCounter: IDCounter): NFA {
            return NFA.ofConcat(left.toNFA(idCounter), right.toNFA(idCounter))
        }

        override fun getAlphabet(out: MutableSet<Char>) {
            left.getAlphabet(out)
            right.getAlphabet(out)
        }
    }

    /**
     * Concat
     */
    protected operator fun TokenSymbol.rangeTo(other: TokenSymbol): TokenSymbol {
        return Sequence(this, other)
    }


    private class ZeroOrOne(val symbol: TokenSymbol) : TokenSymbol {
        override fun toString(): String {
            return "$symbol?"
        }

        override fun toNFA(idCounter: IDCounter): NFA {
            return NFA.ofZeroOrOne(symbol.toNFA(idCounter), idCounter)
        }

        override fun getAlphabet(out: MutableSet<Char>) {
            symbol.getAlphabet(out)
        }
    }

    /**
     * Optional
     */
    protected val TokenSymbol.zeroOrOne: TokenSymbol get() = ZeroOrOne(this)


    private class ZeroOrMore(val symbol: TokenSymbol) : TokenSymbol {
        override fun toString(): String {
            return "$symbol*"
        }

        override fun toNFA(idCounter: IDCounter): NFA {
            return NFA.ofKleeneClosure(symbol.toNFA(idCounter), idCounter)
        }

        override fun getAlphabet(out: MutableSet<Char>) {
            symbol.getAlphabet(out)
        }
    }

    /**
     * Kleene closure
     */
    protected val TokenSymbol.zeroOrMore: TokenSymbol get() = ZeroOrMore(this)


    private class OneOrMore(val symbol: TokenSymbol) : TokenSymbol {
        override fun toString(): String {
            return "$symbol+"
        }

        override fun toNFA(idCounter: IDCounter): NFA {
            return NFA.ofOneOrMore(symbol.toNFA(idCounter), idCounter)
        }

        override fun getAlphabet(out: MutableSet<Char>) {
            symbol.getAlphabet(out)
        }
    }

    /**
     * One-ore-more
     */
    protected val TokenSymbol.oneOrMore: TokenSymbol get() = OneOrMore(this)


    protected object N {
        sealed class CountNumber {
            object NCount : CountNumber() {
                override fun toString() = "n"
            }
            object ZeroCount : CountNumber() {
                override fun toString() = "0"
            }
            object OneCount : CountNumber() {
                override fun toString() = "1"
            }
        }
        data class Count(
            val first: CountNumber,
            val last: CountNumber
        )

        val n = CountNumber.NCount
        val zero = CountNumber.ZeroCount
        val one = CountNumber.OneCount

        operator fun CountNumber.rangeTo(end: CountNumber): Count {
            return Count(this, end)
        }

        operator fun Int.rangeTo(end: CountNumber): Count {
            return when (this) {
                0 -> zero..end
                1 -> one..end
                else -> throw RuntimeException("invalid range ($this .. $end)")
            }
        }

        operator fun get(symbol: TokenSymbol): (N.() -> Count) -> TokenSymbol {
            return { count ->
                val c = count(this)
                getSymbol(c, symbol)
            }
        }

        private fun getSymbol(c: Count, symbol: TokenSymbol): TokenSymbol {
            if (c.first === zero) {
                if (c.last === n) {
                    return ZeroOrMore(symbol)
                } else if (c.last === one) {
                    return ZeroOrOne(symbol)
                }
            } else if (c.first === one) {
                if (c.last === n) {
                    return OneOrMore(symbol)
                }
            }
            throw RuntimeException("invalid N range: $c")
        }
    }
}
