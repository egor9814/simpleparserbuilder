package ru.egor9814.spb.design

import ru.egor9814.spb.builder.fsm.IDCounter
import ru.egor9814.spb.builder.fsm.NFA

interface TokenSymbol {
    fun toNFA(idCounter: IDCounter): NFA

    fun getAlphabet(out: MutableSet<Char>)
}
