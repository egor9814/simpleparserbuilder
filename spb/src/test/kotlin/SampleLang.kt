import ru.egor9814.spb.builder.ParserBuilder
import ru.egor9814.spb.design.LangConfiguration
import ru.egor9814.spb.design.LexerConfiguration
import ru.egor9814.spb.design.ParserConfiguration
import java.io.File

class SampleLexer : LexerConfiguration() {

    private val digit by fragment {
        T["0-9"]
    }

    val int by token {
        digit .. N[ digit or T['_'] ]{0..n}
    }

    val hexInt by token {
        T("0x") .. N[ T["0-9a-fA-F_"] ]{1..n}
    }

    val octInt by token {
        T("0o") .. N[ T["0-7_"] ]{1..n}
    }

    val binInt by token {
        T("0b") .. N[ T["01_"] ]{1..n}
    }

    val float by token {
        ( N[T{int}]{zero..one} .. T['.'] .. T{int} ) or ( T{int} .. T["fF"] )
    }


    private val idStart by fragment {
        T["a-zA-Z_$"]
    }
    private val idPart by fragment {
        idStart or digit
    }
    val id by token {
        idStart .. N[idPart]{0..n}
    }


    val plus by token("+")
    val minus by token("-")
    val star by token("*")
    val star2 by token("**")
    val slash by token("/")
    val backSlash by token("\\")

    val lparen by token("(")
    val rparen by token(")")
    val lbracket by token("[")
    val rbracket by token("]")
    val lbrace by token("{")
    val rbrace by token("}")

    val comma by token(",")

    private val ws by token {
        T[" \r\n\t"]
    }

    init {
        ws.skip = true
    }
}


class SampleParser : ParserConfiguration<SampleLexer>(SampleLexer()) {

    init {
        rule("id") {
            T{id}
        }

        rule("int") {
            T{int} or T{hexInt} or T{octInt} or T{binInt}
        }

        rule("float") {
            T{float}
        }

        rule("literal") {
            R{"int"} or R{"float"}
        }

        rule("simple") {
            R{"literal"} or R{"id"} or ( T{lparen} .. R{"expr"} .. T{rparen} )
        }

        rule("callArgs") {
            R{"expr"} .. N[ T{comma} .. R{"expr"} ]{0..n}
        }

        rule("call") {
            R{"simple"} .. N[ T{lparen}.. R{"callArgs"}.zeroOrOne .. T{rparen} ]{0..n}
        }

        rule("multOperator") {
            T{star} or T{star2} or T{slash} or T{backSlash}
        }
        rule("mult") {
            R{"call"} .. N[ R{"multOperator"} .. R{"call"} ]{0..n}
        }

        rule("addOperator") {
            T{plus} or T{minus}
        }
        rule("add") {
            R{"mult"} .. N[ R{"addOperator"} .. R{"mult"} ]{0..n}
        }

        rule("expr") {
            R{"add"}
        }

        rule("main") {
            N[ R{"expr"} ]{0..n}
        }
    }
}


class SampleLang : LangConfiguration() {

    init {
        author = arrayOf("egor9814")
        packageName = "sample"
        langName = "SampleLang"

        setParserRules(SampleParser()) {
            "main"
        }
    }
}


object SampleLangGenerator {
    @JvmStatic
    fun main(args: Array<String>) {
        val wd = File("").absoluteFile
        val out = File(wd, "spb_lib/src/test/kotlin")
        val builder = ParserBuilder(SampleLang())
        builder.build(out)
    }
}
