package ru.egor9814.spb.lib.parser

import ru.egor9814.spb.lib.common.Position

/**
 * Parse error
 * @param position Where happens error
 * @param message Error text
 */
class ParseException(val position: Position, message: String) : RuntimeException("$position $message")
