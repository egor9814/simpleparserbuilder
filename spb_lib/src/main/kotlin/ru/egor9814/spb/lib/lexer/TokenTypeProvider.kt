package ru.egor9814.spb.lib.lexer

/**
 * TokenType Abstract Enumeration
 */
abstract class TokenTypeProvider {

    private val tokens = mutableMapOf<String, ITokenType>()

    protected fun register(tokenType: ITokenType) {
        tokens[tokenType.name] = tokenType
    }

    fun values() = tokens.values.toSet()

    fun valueOf(name: String) = tokens[name]


    abstract class SimpleTokenType(
        tokenName: String,
        ordinalValue: Int
    ) : ITokenType {
        override val name = tokenName
        override val ordinal = ordinalValue

        override fun equals(other: Any?): Boolean {
            return other is ITokenType && other.name == name && other.ordinal == ordinal
        }

        override fun hashCode(): Int {
            return 31 * (31 * name.hashCode() + ordinal.hashCode())
        }

        override fun toString(): String {
            return "[$name]"
        }
    }

}
