package ru.egor9814.spb.lib.lexer

import ru.egor9814.spb.lib.common.Position

data class Token(
    val value: String,
    val startPosition: Position,
    val endPosition: Position,
    val symbol: Symbol
) {
    override fun toString(): String {
        return "$startPosition-$endPosition: ${symbol.tokenType} - \"${wrapEscapeString(
            value
        )}\""
    }

    companion object {
        fun wrapEscapeString(str: String): String {
            val buf = StringBuilder()
            for (char in str) {
                when (char) {
                    '\r' -> buf.append("\\r")
                    '\n' -> buf.append("\\n")
                    '\t' -> buf.append("\\t")
                    '\\' -> buf.append("\\\\")
                    '\'' -> buf.append("\\'")
                    '\"' -> buf.append("\\\"")
                    else -> buf.append(char)
                }
            }
            return buf.toString()
        }
    }

}