package ru.egor9814.spb.lib.lexer

import ru.egor9814.spb.lib.common.IInputCharStream
import ru.egor9814.spb.lib.common.Position

/**
 * Source code
 */
data class SourceCode(val code: String) {

    /**
     * Tab size: usual = 4 or 8
     */
    var tabSize = 4

    private var row = -1
    private var col = -1

    /**
     * Row and col value in source code
     */
    var codePosition: Position
        get() = Position(row, col)
        internal set(value) {
            row = value.row
            col = value.col
        }


    var position = -1
        internal set

    internal fun reset() {
        row = 1
        col = 1
        position = 0
    }


    fun available() = code.length - position


    internal fun getBuffer(count: Int): String {
        val size = code.length - position
        return if (count > size)
            code.substring(position)
        else
            code.substring(position, count + position)
    }

    internal operator fun get(index: Int): Char {
        val i = position + index
        return if (i >= code.length)
            '\u0000'
        else
            code[i]
    }

    internal fun consume(text: String): Boolean {
        val buf = getBuffer(text.length)
        if (buf == text) {
            for (char in text) {
                when (char) {
                    '\n' -> { row++; col = 1 }
                    '\r' -> {}
                    '\t' -> col += tabSize
                    else -> col += 1
                }
            }
            position += text.length
            return true
        }
        return false
    }

    companion object {
        fun of(input: IInputCharStream): SourceCode {
            val buf = StringBuilder()
            if (input.open()) {
                while (input.hasNext())
                    buf.append(input.next())
                input.close()
            }
            return SourceCode(buf.toString())
        }
    }

}
