package ru.egor9814.spb.lib.parser

import ru.egor9814.spb.lib.lexer.ITokenType
import ru.egor9814.spb.lib.lexer.Token
import ru.egor9814.spb.lib.lexer.TokenTypeProvider

/**
 * Abstract parser
 * @param tokens List of tokens
 * @param tokenTypeProvider TokenType set
 */
abstract class ParserBase<TT : TokenTypeProvider>(
    private val tokens: List<Token>,
    tokenTypeProvider: TT
) {
    protected val TokenType = tokenTypeProvider

    private val EOFType = tokenTypeProvider.valueOf("EOF")!!

    private var pos = 0

    fun reset() {
        pos = 0
    }

    private fun get(): Token {
        return if (pos >= tokens.size) tokens.last() else tokens[pos]
    }

    /**
     * Check current tokenType with 't'
     * @return true, if types equals and move to next token
     */
    protected fun check(t: ITokenType, node: INode? = null): Boolean {
        val token = get()
        if (token.symbol.tokenType == t) {
            if (node is Node)
                node.addChild(TokenNode(token))
            pos++
            return true
        }
        return false
    }


    /**
     * Main parsing rule
     */
    protected abstract fun main(result: Node): Boolean


    /**
     * Dummy node for parsing
     */
    private class ResultNode : Node("Result")

    /**
     * Parse tokens
     */
    fun parse(): ParseResult {
        val result = ResultNode()
        var errorMessage = ""
        val success = try {
            var res = main(result)
            if (!res) {
                errorMessage = "Unknown parse error at ${get().startPosition}"
            } else if (get().symbol.tokenType != EOFType) {
                res = false
                errorMessage = "Unexpected symbol: ${get()}"
            }
            res
        } catch (err: Throwable) {
            errorMessage = err.message ?: "<unknown error>"
            false
        }
        return if (success)
            result.firstOrNull()?.let {
                ParseResult.Success(it)
            } ?: ParseResult.Failed("Invalid parse node type")
        else
            ParseResult.Failed(errorMessage)
    }


    protected fun error(message: String): Boolean {
        throw ParseException((if (pos >= tokens.size) tokens.last() else tokens[pos]).startPosition, message)
    }


    /*protected fun checkSource(state: Int?): Boolean {
        if (state != null && pos != state) {
            error("Invalid symbol sequence")
        }
        return false
    }*/

    protected fun checked(message: String?, action: () -> Boolean): Boolean {
        val res = action()
        if (!res) {
            val state = StateStack.peek()
            if (state != null && state != pos) {
                return error(message ?: "<unknown error at ${tokens[pos].startPosition}>")
            }
        }
        return res
    }

    protected fun check(message: String?) {
        val state = StateStack.peek()
        if (state != null && state != pos) {
            error(message ?: "<unknown error at ${tokens[pos].startPosition}>")
        }
    }

    protected fun save() = apply {
        StateStack.push(pos)
    }

    protected fun popState() = apply {
        StateStack.pop()
    }

    private object StateStack {
        private val states = mutableListOf<Int>()

        fun push(value: Int) = states.add(0, value)

        fun pop() {
            if (states.isNotEmpty())
                states.removeAt(0)
        }

        fun peek() = states.firstOrNull()
    }

}
