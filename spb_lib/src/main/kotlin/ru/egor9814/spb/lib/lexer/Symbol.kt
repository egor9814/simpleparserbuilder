package ru.egor9814.spb.lib.lexer

enum class SymbolType {
    Token,
    Error,
    End,
    Skip
}

data class Symbol(
    val symbolType: SymbolType,
    val tokenType: ITokenType
)
