package ru.egor9814.spb.lib.lexer

/**
 * Class for restore transition table by bytes
 */
internal class DFAReader(val bytes: ByteArray) {

    private var pos = 0

    private fun hasNextByte() = pos < bytes.size

    private fun nextByte() = if (hasNextByte()) bytes[pos++] else 0

    private fun nextInt(count: Int = 4): Int {
        var result = 0
        for (i in 0 until count) {
            result = result or (nextByte().toInt() shl (8 * i))
        }
        return result
    }

    private fun nextShort() = nextInt(2).toShort()

    private fun nextChar() = nextShort().toChar()

    private fun nextString(): String {
        val len = nextInt()
        val res = StringBuilder()
        for (i in 0 until len) {
            res.append(nextChar())
        }
        return res.toString()
    }

    fun read(tokenTypeProvider: TokenTypeProvider): List<State> {
        val result = mutableListOf<State>()

        val transitions = mutableMapOf<Int, MutableMap<Int, MutableSet<Char>>>()
        for (i in 0 until nextInt()) {
            val state = nextInt()
            val stateTransitions = mutableMapOf<Int, MutableSet<Char>>()
            for (j in 0 until nextInt()) {
                val target = nextInt()
                val chars = mutableSetOf<Char>().also {
                    stateTransitions[target] = it
                }
                for (k in 0 until nextInt()) {
                    chars.add(nextChar())
                }
            }
            transitions[state] = stateTransitions
        }

        for (i in 0 until nextInt()) {
            val id = nextInt()

            val s = if (nextByte().toInt() == 1) {
                val type = if (nextByte().toInt() == 1) SymbolType.Skip else SymbolType.Token
                val name = nextString()
                Symbol(type, tokenTypeProvider.valueOf(name)!!)
            } else {
                null
            }

            result.add(
                State(
                    transitions[id]!!.map { (k, v) ->
                        Edge(
                            v.toSet(),
                            k
                        )
                    }.toList(),
                    s
                )
            )
        }

        return result
    }

}
