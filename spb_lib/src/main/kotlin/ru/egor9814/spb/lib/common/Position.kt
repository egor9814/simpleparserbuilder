package ru.egor9814.spb.lib.common

data class Position(val row: Int = 0, val col: Int = 0) {
    override fun toString(): String {
        return "[$row:$col]"
    }

    operator fun rangeTo(other: Position) = PositionRange(this, other)
}

data class PositionRange(
    val start: Position = Position(),
    val end: Position = Position()
) {
    override fun toString(): String {
        return "$start-$end"
    }
}
