package ru.egor9814.spb.lib.parser

sealed class ParseResult {

    class Success(val node: INode) : ParseResult() {
        inline fun <reified N : INode> castOrNull(): N? {
            return if (node is N) node else null
        }
    }

    class Failed(val message: String) : ParseResult()

}
