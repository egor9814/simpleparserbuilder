package ru.egor9814.spb.lib.lexer

/**
 * Transition edge from 'current state' to 'target' by one char from 'chars'
 */
internal data class Edge(
    val chars: Set<Char>,
    val target: Int
) {
    override fun toString(): String {
        return "'${chars.joinToString(prefix = "{", postfix = "}") {
            Token.wrapEscapeString("$it")
        }
        }' -> ($target)"
    }
}
