package ru.egor9814.spb.lib.parser

interface Visitable {

    fun <A> apply(visitor: Visitor<A>, arg: A)

}
