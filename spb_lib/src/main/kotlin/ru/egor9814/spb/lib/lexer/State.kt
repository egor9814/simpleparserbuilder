package ru.egor9814.spb.lib.lexer

/**
 * State of finite machine
 * @param edges Possible transitions
 * @param accept Flag of acceptable state
 */
internal data class State(
    val edges: List<Edge>,
    val accept: Symbol?
)
