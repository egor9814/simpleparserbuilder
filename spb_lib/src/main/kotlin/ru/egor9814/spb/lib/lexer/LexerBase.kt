package ru.egor9814.spb.lib.lexer

/**
 * Lexical analyzer
 * @param sourceCode Source code
 * @param tokenTypeProvider TokenType set
 * @param dfaBytes DFA bytes
 */
abstract class LexerBase(
    private val sourceCode: SourceCode,
    tokenTypeProvider: TokenTypeProvider,
    vararg dfaBytes: Byte
) {
    private val EOFType = tokenTypeProvider.valueOf("EOF")!!
    private val ErrorType = tokenTypeProvider.valueOf("ERROR")!!

    private val dfa = DFAReader(dfaBytes).read(tokenTypeProvider)
    private val tokens = mutableListOf<Token>()

    init {
        sourceCode.reset()
    }

    fun reset() {
        sourceCode.reset()
    }

    /**
     * Read token by simulating DFA moving
     */
    private fun readToken(): Token {
        var lastAcceptPosition = -1
        var lastAcceptState = -1
        var pos = 0
        var currentState = 0

        var text = ""
        val symbol: Symbol?
        val position = sourceCode.codePosition

        if (sourceCode[0] == '\u0000') {
            symbol = Symbol(
                SymbolType.End,
                EOFType
            )
        } else while (true) {
            val char = sourceCode[pos]
            var target = -1
            if (char != '\u0000') {
                for (edge in dfa[currentState].edges) {
                    if (char in edge.chars) {
                        target = edge.target
                        break
                    }
                }
            }

            if (target == -1) {
                if (lastAcceptState == -1) {
                    text = sourceCode.getBuffer(1)
                    symbol = Symbol(
                        SymbolType.Error,
                        ErrorType
                    )
                } else {
                    text = sourceCode.getBuffer(lastAcceptPosition + 1)
                    symbol = dfa[lastAcceptState].accept
                }
                break
            } else {
                if (dfa[target].accept != null) {
                    lastAcceptState = target
                    lastAcceptPosition = pos
                }
                currentState = target
                pos++
            }
        }

        sourceCode.consume(text)
        return Token(text, position, sourceCode.codePosition, symbol!!)
    }

    /**
     * Next unskippable token
     */
    fun nextToken(): Token {
        var token: Token
        do {
            token = readToken()
            if (token.symbol.symbolType == SymbolType.Error)
                throw LexerException("cannot read token at ${token.startPosition}")
        } while (token.symbol.symbolType == SymbolType.Skip)
        tokens.add(token)
        return token
    }

    /**
     * Get all tokenized tokens
     */
    fun getTokens() = tokens.toList()

    /**
     * Read all tokens
     */
    fun tokenize(): List<Token> {
        var token = nextToken()
        while (token.symbol.symbolType != SymbolType.End) {
            token = nextToken()
        }
        return tokens.toList()
    }

}
