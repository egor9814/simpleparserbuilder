package ru.egor9814.spb.lib.parser

interface INode : Visitable {
    val isTerminal: Boolean
}
