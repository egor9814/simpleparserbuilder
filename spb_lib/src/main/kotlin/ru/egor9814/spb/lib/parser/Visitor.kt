package ru.egor9814.spb.lib.parser

interface Visitor<A> {

    fun visit(node: INode, arg: A)

}
