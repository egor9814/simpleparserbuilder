package ru.egor9814.spb.lib.common

interface IInputCharStream {

    fun open(): Boolean

    fun close()

    fun hasNext(): Boolean

    fun next(): Char

}