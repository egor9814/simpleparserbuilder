package ru.egor9814.spb.lib.parser

import ru.egor9814.spb.lib.lexer.Token
import java.util.*

/**
 * Default Parse tree node
 */
abstract class Node(val name: String) : INode, Iterable<INode> {

    override val isTerminal = false

    private val mChildren = mutableListOf<INode>()

    fun addChild(node: INode) = apply {
        mChildren.add(node)
    }

    val children get() = mChildren.toList()

    val size get() = mChildren.size

    fun isNotEmpty() = size != 0

    fun isEmpty() = size == 0

    override fun iterator(): Iterator<INode> {
        return mChildren.iterator()
    }

    override fun spliterator(): Spliterator<INode> {
        return mChildren.spliterator()
    }

    override fun <A> apply(visitor: Visitor<A>, arg: A) {
        visitor.visit(this, arg)
    }

    override fun toString(): String {
        return joinToString(separator = " .. ", prefix = "\"$name\"{ ", postfix = " }\"$name\"") {
            it.toString()
        }
    }
}


/**
 * Terminal parse tree node
 */
class TokenNode(val value: Token) : INode {
    override val isTerminal = true

    override fun <A> apply(visitor: Visitor<A>, arg: A) {
        visitor.visit(this, arg)
    }

    override fun toString(): String {
        return "'${value.value}'"
    }
}
