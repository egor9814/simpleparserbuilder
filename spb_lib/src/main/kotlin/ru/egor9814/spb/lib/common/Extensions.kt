package ru.egor9814.spb.lib.common

import ru.egor9814.spb.lib.lexer.SourceCode
import java.io.File
import java.io.InputStream

/**
 * Implementation of IInputCharStream for standard java.io.InputStream
 */
class InputCharStream(private val inputStreamCreator: () -> InputStream) :
    IInputCharStream {
    private var input: InputStream? = null

    override fun open(): Boolean {
        if (input == null) {
            try {
                input = inputStreamCreator()
            } finally {}
        }
        return input != null
    }

    override fun close() {
        input?.close()
        input = null
    }

    override fun hasNext(): Boolean {
        return (input?.available() ?: 0) > 0
    }

    override fun next(): Char {
        return input?.read()?.toChar() ?: '\u0000'
    }
}

/**
 * Convert file to Source code
 */
fun File.toSourceCode(): SourceCode {
    return SourceCode.of(InputCharStream { this.inputStream() })
}
