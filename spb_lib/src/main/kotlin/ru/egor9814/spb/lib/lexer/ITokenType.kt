package ru.egor9814.spb.lib.lexer

interface ITokenType {
    val name: String
    val ordinal: Int
}
