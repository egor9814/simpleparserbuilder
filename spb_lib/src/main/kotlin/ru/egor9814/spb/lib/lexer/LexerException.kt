package ru.egor9814.spb.lib.lexer

class LexerException(message: String) : RuntimeException(message)
