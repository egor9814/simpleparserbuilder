package ru.egor9814.spb.lib.parser

import java.io.PrintStream

/**
 * Print parse tree as meta-XML formal
 */
class PrettyPrintNodeTree(val out: PrintStream) : Visitor<Int> {

    companion object {
        fun print(node: INode, out: PrintStream) = PrettyPrintNodeTree(out).apply {
            node.apply(this, 0)
        }
    }

    private fun printIndent(indentLevel: Int) = apply {
        for (i in 1..indentLevel) {
            out.print("  ")
        }
    }

    override fun visit(node: INode, arg: Int) {
        out.println()
        printIndent(arg)
        if (node.isTerminal) {
            (node as TokenNode).value.value.let(out::print)
        } else {
            (node as Node).apply {
                out.print("<$name>")
                forEach {
                    it.apply(this@PrettyPrintNodeTree, arg + 1)
                }
                out.println()
                printIndent(arg)
                out.print("</$name>")
            }
        }
    }

}
