/* Auto-Generated Recursive Descent Parser by egor9814 */

package sample

import ru.egor9814.spb.lib.lexer.*

import ru.egor9814.spb.lib.parser.*

class SampleLangParser(tokens: List<Token>) : ParserBase<SampleLangTokenType>(tokens, SampleLangTokenType) {
    class IdNode : Node("Id")
    private fun rid(parentNode: Node?): Boolean {
        val rn = IdNode()
        var r = true
        r = check(TokenType.Id, rn)
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class IntNode : Node("Int")
    private fun rint(parentNode: Node?): Boolean {
        val rn = IntNode()
        var r = true
        if (r) while (true) {
            save()
            r = checked("Alternative read tokens, but did not complete") {
                check(TokenType.Int, rn)
            }
            if (r) { popState(); break } else r = checked("Alternative read tokens, but did not complete") {
                check(TokenType.HexInt, rn)
            }
            if (r) { popState(); break } else r = checked("Alternative read tokens, but did not complete") {
                check(TokenType.OctInt, rn)
            }
            if (r) { popState(); break } else r = checked("Alternative read tokens, but did not complete") {
                check(TokenType.BinInt, rn)
            }
            if (!r) {
                check("Alternative not selected, but tokens have already been read in one of them")
            }
            popState()
            break
        } else return false
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class FloatNode : Node("Float")
    private fun rfloat(parentNode: Node?): Boolean {
        val rn = FloatNode()
        var r = true
        r = check(TokenType.Float, rn)
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class LiteralNode : Node("Literal")
    private fun rliteral(parentNode: Node?): Boolean {
        val rn = LiteralNode()
        var r = true
        if (r) while (true) {
            save()
            r = checked("Alternative read tokens, but did not complete") {
                rliteral0(rn)
            }
            if (r) { popState(); break } else r = checked("Alternative read tokens, but did not complete") {
                rliteral1(rn)
            }
            if (!r) {
                check("Alternative not selected, but tokens have already been read in one of them")
            }
            popState()
            break
        } else return false
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class SimpleNode : Node("Simple")
    private fun rsimple(parentNode: Node?): Boolean {
        val rn = SimpleNode()
        var r = true
        if (r) while (true) {
            save()
            r = checked("Alternative read tokens, but did not complete") {
                rsimple0(rn)
            }
            if (r) { popState(); break } else r = checked("Alternative read tokens, but did not complete") {
                rsimple1(rn)
            }
            if (r) { popState(); break } else r = checked("Alternative read tokens, but did not complete") {
                rsimple2(rn)
            }
            if (!r) {
                check("Alternative not selected, but tokens have already been read in one of them")
            }
            popState()
            break
        } else return false
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class CallArgsNode : Node("CallArgs")
    private fun rcallArgs(parentNode: Node?): Boolean {
        val rn = CallArgsNode()
        var r = true
        if (!r) return false else r = rexpr(rn)
        if (r) do {
            r = rcallArgs1ZoM(rn)
        } while (r) else return false
        r = true
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class CallNode : Node("Call")
    private fun rcall(parentNode: Node?): Boolean {
        val rn = CallNode()
        var r = true
        if (!r) return false else r = rsimple(rn)
        if (r) do {
            r = rcall1ZoM(rn)
        } while (r) else return false
        r = true
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class MultOperatorNode : Node("MultOperator")
    private fun rmultOperator(parentNode: Node?): Boolean {
        val rn = MultOperatorNode()
        var r = true
        if (r) while (true) {
            save()
            r = checked("Alternative read tokens, but did not complete") {
                check(TokenType.Star, rn)
            }
            if (r) { popState(); break } else r = checked("Alternative read tokens, but did not complete") {
                check(TokenType.Star2, rn)
            }
            if (r) { popState(); break } else r = checked("Alternative read tokens, but did not complete") {
                check(TokenType.Slash, rn)
            }
            if (r) { popState(); break } else r = checked("Alternative read tokens, but did not complete") {
                check(TokenType.BackSlash, rn)
            }
            if (!r) {
                check("Alternative not selected, but tokens have already been read in one of them")
            }
            popState()
            break
        } else return false
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class MultNode : Node("Mult")
    private fun rmult(parentNode: Node?): Boolean {
        val rn = MultNode()
        var r = true
        if (!r) return false else r = rcall(rn)
        if (r) do {
            r = rmult1ZoM(rn)
        } while (r) else return false
        r = true
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class AddOperatorNode : Node("AddOperator")
    private fun raddOperator(parentNode: Node?): Boolean {
        val rn = AddOperatorNode()
        var r = true
        if (r) while (true) {
            save()
            r = checked("Alternative read tokens, but did not complete") {
                check(TokenType.Plus, rn)
            }
            if (r) { popState(); break } else r = checked("Alternative read tokens, but did not complete") {
                check(TokenType.Minus, rn)
            }
            if (!r) {
                check("Alternative not selected, but tokens have already been read in one of them")
            }
            popState()
            break
        } else return false
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class AddNode : Node("Add")
    private fun radd(parentNode: Node?): Boolean {
        val rn = AddNode()
        var r = true
        if (!r) return false else r = rmult(rn)
        if (r) do {
            r = radd1ZoM(rn)
        } while (r) else return false
        r = true
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class ExprNode : Node("Expr")
    private fun rexpr(parentNode: Node?): Boolean {
        val rn = ExprNode()
        var r = true
        if (!r) return false else r = radd(rn)
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    class MainNode : Node("Main")
    private fun rmain(parentNode: Node?): Boolean {
        val rn = MainNode()
        var r = true
        if (r) do {
            r = rmainZoM(rn)
        } while (r) else return false
        r = true
        if (r && parentNode is Node) parentNode.addChild(rn)
        return r
    }
    private fun rliteral0(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        if (!r) return false else r = rint(rn)
        return r
    }
    private fun rliteral1(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        if (!r) return false else r = rfloat(rn)
        return r
    }
    private fun rsimple0(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        if (!r) return false else r = rliteral(rn)
        return r
    }
    private fun rsimple1(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        if (!r) return false else r = rid(rn)
        return r
    }
    private fun rsimple2(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        if (!r) return false else r = check(TokenType.Lparen, rn)
        if (!r) return false else r = rexpr(rn)
        if (!r) return false else r = check(TokenType.Rparen, rn)
        return r
    }
    private fun rcallArgs1ZoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        if (!r) return false else r = check(TokenType.Comma, rn)
        if (!r) return false else r = rexpr(rn)
        return r
    }
    private fun rcall1ZoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        if (!r) return false else r = check(TokenType.Lparen, rn)
        if (r) {
            r = rcall1ZoM1ZoO(rn)
        } else return false
        r = true
        if (!r) return false else r = check(TokenType.Rparen, rn)
        return r
    }
    private fun rmult1ZoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        if (!r) return false else r = rmultOperator(rn)
        if (!r) return false else r = rcall(rn)
        return r
    }
    private fun radd1ZoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        if (!r) return false else r = raddOperator(rn)
        if (!r) return false else r = rmult(rn)
        return r
    }
    private fun rmainZoM(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        if (!r) return false else r = rexpr(rn)
        return r
    }
    private fun rcall1ZoM1ZoO(parentNode: Node?): Boolean {
        val rn = parentNode
        var r = true
        if (!r) return false else r = rcallArgs(rn)
        return r
    }
    override fun main(result: Node): Boolean {
        return rmain(result)
    }
}
