/* Auto-Generated Recursive Descent Parser by egor9814 */

package sample

import ru.egor9814.spb.lib.lexer.*

class SampleLangLexer(source: SourceCode) : LexerBase(source, SampleLangTokenType,
    19.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    13.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 3.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    4.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 32.toByte(), 0.toByte(), 13.toByte(), 0.toByte(),
    10.toByte(), 0.toByte(), 9.toByte(), 0.toByte(), 4.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    54.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 97.toByte(), 0.toByte(), 98.toByte(), 0.toByte(),
    99.toByte(), 0.toByte(), 100.toByte(), 0.toByte(), 101.toByte(), 0.toByte(), 102.toByte(), 0.toByte(),
    103.toByte(), 0.toByte(), 104.toByte(), 0.toByte(), 105.toByte(), 0.toByte(), 106.toByte(), 0.toByte(),
    107.toByte(), 0.toByte(), 108.toByte(), 0.toByte(), 109.toByte(), 0.toByte(), 110.toByte(), 0.toByte(),
    111.toByte(), 0.toByte(), 112.toByte(), 0.toByte(), 113.toByte(), 0.toByte(), 114.toByte(), 0.toByte(),
    115.toByte(), 0.toByte(), 116.toByte(), 0.toByte(), 117.toByte(), 0.toByte(), 118.toByte(), 0.toByte(),
    119.toByte(), 0.toByte(), 120.toByte(), 0.toByte(), 121.toByte(), 0.toByte(), 122.toByte(), 0.toByte(),
    65.toByte(), 0.toByte(), 66.toByte(), 0.toByte(), 67.toByte(), 0.toByte(), 68.toByte(), 0.toByte(),
    69.toByte(), 0.toByte(), 70.toByte(), 0.toByte(), 71.toByte(), 0.toByte(), 72.toByte(), 0.toByte(),
    73.toByte(), 0.toByte(), 74.toByte(), 0.toByte(), 75.toByte(), 0.toByte(), 76.toByte(), 0.toByte(),
    77.toByte(), 0.toByte(), 78.toByte(), 0.toByte(), 79.toByte(), 0.toByte(), 80.toByte(), 0.toByte(),
    81.toByte(), 0.toByte(), 82.toByte(), 0.toByte(), 83.toByte(), 0.toByte(), 84.toByte(), 0.toByte(),
    85.toByte(), 0.toByte(), 86.toByte(), 0.toByte(), 87.toByte(), 0.toByte(), 88.toByte(), 0.toByte(),
    89.toByte(), 0.toByte(), 90.toByte(), 0.toByte(), 95.toByte(), 0.toByte(), 36.toByte(), 0.toByte(),
    1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    48.toByte(), 0.toByte(), 2.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 9.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 49.toByte(), 0.toByte(), 50.toByte(), 0.toByte(), 51.toByte(), 0.toByte(),
    52.toByte(), 0.toByte(), 53.toByte(), 0.toByte(), 54.toByte(), 0.toByte(), 55.toByte(), 0.toByte(),
    56.toByte(), 0.toByte(), 57.toByte(), 0.toByte(), 18.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 46.toByte(), 0.toByte(), 5.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 40.toByte(), 0.toByte(),
    6.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    41.toByte(), 0.toByte(), 7.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 44.toByte(), 0.toByte(), 8.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 42.toByte(), 0.toByte(), 9.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 47.toByte(), 0.toByte(),
    10.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    92.toByte(), 0.toByte(), 11.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 43.toByte(), 0.toByte(), 12.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 45.toByte(), 0.toByte(), 1.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 3.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 18.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 4.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 98.toByte(), 0.toByte(),
    111.toByte(), 0.toByte(), 120.toByte(), 0.toByte(), 46.toByte(), 0.toByte(), 13.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 2.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 102.toByte(), 0.toByte(),
    70.toByte(), 0.toByte(), 2.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 11.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 95.toByte(), 0.toByte(), 48.toByte(), 0.toByte(), 49.toByte(), 0.toByte(),
    50.toByte(), 0.toByte(), 51.toByte(), 0.toByte(), 52.toByte(), 0.toByte(), 53.toByte(), 0.toByte(),
    54.toByte(), 0.toByte(), 55.toByte(), 0.toByte(), 56.toByte(), 0.toByte(), 57.toByte(), 0.toByte(),
    2.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 3.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    13.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 2.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    102.toByte(), 0.toByte(), 70.toByte(), 0.toByte(), 2.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    11.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 95.toByte(), 0.toByte(), 48.toByte(), 0.toByte(),
    49.toByte(), 0.toByte(), 50.toByte(), 0.toByte(), 51.toByte(), 0.toByte(), 52.toByte(), 0.toByte(),
    53.toByte(), 0.toByte(), 54.toByte(), 0.toByte(), 55.toByte(), 0.toByte(), 56.toByte(), 0.toByte(),
    57.toByte(), 0.toByte(), 18.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 46.toByte(), 0.toByte(), 3.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 4.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 4.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    64.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 97.toByte(), 0.toByte(), 98.toByte(), 0.toByte(),
    99.toByte(), 0.toByte(), 100.toByte(), 0.toByte(), 101.toByte(), 0.toByte(), 102.toByte(), 0.toByte(),
    103.toByte(), 0.toByte(), 104.toByte(), 0.toByte(), 105.toByte(), 0.toByte(), 106.toByte(), 0.toByte(),
    107.toByte(), 0.toByte(), 108.toByte(), 0.toByte(), 109.toByte(), 0.toByte(), 110.toByte(), 0.toByte(),
    111.toByte(), 0.toByte(), 112.toByte(), 0.toByte(), 113.toByte(), 0.toByte(), 114.toByte(), 0.toByte(),
    115.toByte(), 0.toByte(), 116.toByte(), 0.toByte(), 117.toByte(), 0.toByte(), 118.toByte(), 0.toByte(),
    119.toByte(), 0.toByte(), 120.toByte(), 0.toByte(), 121.toByte(), 0.toByte(), 122.toByte(), 0.toByte(),
    65.toByte(), 0.toByte(), 66.toByte(), 0.toByte(), 67.toByte(), 0.toByte(), 68.toByte(), 0.toByte(),
    69.toByte(), 0.toByte(), 70.toByte(), 0.toByte(), 71.toByte(), 0.toByte(), 72.toByte(), 0.toByte(),
    73.toByte(), 0.toByte(), 74.toByte(), 0.toByte(), 75.toByte(), 0.toByte(), 76.toByte(), 0.toByte(),
    77.toByte(), 0.toByte(), 78.toByte(), 0.toByte(), 79.toByte(), 0.toByte(), 80.toByte(), 0.toByte(),
    81.toByte(), 0.toByte(), 82.toByte(), 0.toByte(), 83.toByte(), 0.toByte(), 84.toByte(), 0.toByte(),
    85.toByte(), 0.toByte(), 86.toByte(), 0.toByte(), 87.toByte(), 0.toByte(), 88.toByte(), 0.toByte(),
    89.toByte(), 0.toByte(), 90.toByte(), 0.toByte(), 95.toByte(), 0.toByte(), 36.toByte(), 0.toByte(),
    48.toByte(), 0.toByte(), 49.toByte(), 0.toByte(), 50.toByte(), 0.toByte(), 51.toByte(), 0.toByte(),
    52.toByte(), 0.toByte(), 53.toByte(), 0.toByte(), 54.toByte(), 0.toByte(), 55.toByte(), 0.toByte(),
    56.toByte(), 0.toByte(), 57.toByte(), 0.toByte(), 5.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 6.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 7.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 8.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 14.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 42.toByte(), 0.toByte(), 9.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 10.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 11.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 12.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 13.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 13.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 11.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 95.toByte(), 0.toByte(),
    48.toByte(), 0.toByte(), 49.toByte(), 0.toByte(), 50.toByte(), 0.toByte(), 51.toByte(), 0.toByte(),
    52.toByte(), 0.toByte(), 53.toByte(), 0.toByte(), 54.toByte(), 0.toByte(), 55.toByte(), 0.toByte(),
    56.toByte(), 0.toByte(), 57.toByte(), 0.toByte(), 14.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 15.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 15.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    3.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 95.toByte(), 0.toByte(), 48.toByte(), 0.toByte(),
    49.toByte(), 0.toByte(), 16.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 16.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 9.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 95.toByte(), 0.toByte(), 48.toByte(), 0.toByte(), 49.toByte(), 0.toByte(),
    50.toByte(), 0.toByte(), 51.toByte(), 0.toByte(), 52.toByte(), 0.toByte(), 53.toByte(), 0.toByte(),
    54.toByte(), 0.toByte(), 55.toByte(), 0.toByte(), 17.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 17.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    23.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 97.toByte(), 0.toByte(), 98.toByte(), 0.toByte(),
    99.toByte(), 0.toByte(), 100.toByte(), 0.toByte(), 101.toByte(), 0.toByte(), 102.toByte(), 0.toByte(),
    65.toByte(), 0.toByte(), 66.toByte(), 0.toByte(), 67.toByte(), 0.toByte(), 68.toByte(), 0.toByte(),
    69.toByte(), 0.toByte(), 70.toByte(), 0.toByte(), 95.toByte(), 0.toByte(), 48.toByte(), 0.toByte(),
    49.toByte(), 0.toByte(), 50.toByte(), 0.toByte(), 51.toByte(), 0.toByte(), 52.toByte(), 0.toByte(),
    53.toByte(), 0.toByte(), 54.toByte(), 0.toByte(), 55.toByte(), 0.toByte(), 56.toByte(), 0.toByte(),
    57.toByte(), 0.toByte(), 18.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 3.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 17.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 12.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 97.toByte(), 0.toByte(), 98.toByte(), 0.toByte(), 99.toByte(), 0.toByte(),
    100.toByte(), 0.toByte(), 101.toByte(), 0.toByte(), 102.toByte(), 0.toByte(), 65.toByte(), 0.toByte(),
    66.toByte(), 0.toByte(), 67.toByte(), 0.toByte(), 68.toByte(), 0.toByte(), 69.toByte(), 0.toByte(),
    70.toByte(), 0.toByte(), 15.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(),
    0.toByte(), 0.toByte(), 95.toByte(), 0.toByte(), 13.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    10.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 48.toByte(), 0.toByte(), 49.toByte(), 0.toByte(),
    50.toByte(), 0.toByte(), 51.toByte(), 0.toByte(), 52.toByte(), 0.toByte(), 53.toByte(), 0.toByte(),
    54.toByte(), 0.toByte(), 55.toByte(), 0.toByte(), 56.toByte(), 0.toByte(), 57.toByte(), 0.toByte(),
    19.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 1.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 3.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 73.toByte(), 0.toByte(), 110.toByte(), 0.toByte(), 116.toByte(),
    0.toByte(), 2.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 3.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 73.toByte(), 0.toByte(), 110.toByte(), 0.toByte(), 116.toByte(),
    0.toByte(), 3.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 1.toByte(), 2.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 87.toByte(), 0.toByte(), 115.toByte(), 0.toByte(), 4.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 2.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 73.toByte(), 0.toByte(), 100.toByte(), 0.toByte(), 5.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 1.toByte(), 0.toByte(), 6.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 76.toByte(),
    0.toByte(), 112.toByte(), 0.toByte(), 97.toByte(), 0.toByte(), 114.toByte(), 0.toByte(), 101.toByte(),
    0.toByte(), 110.toByte(), 0.toByte(), 6.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(),
    0.toByte(), 6.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 82.toByte(), 0.toByte(), 112.toByte(),
    0.toByte(), 97.toByte(), 0.toByte(), 114.toByte(), 0.toByte(), 101.toByte(), 0.toByte(), 110.toByte(),
    0.toByte(), 7.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 5.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 67.toByte(), 0.toByte(), 111.toByte(), 0.toByte(), 109.toByte(),
    0.toByte(), 109.toByte(), 0.toByte(), 97.toByte(), 0.toByte(), 8.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 1.toByte(), 0.toByte(), 4.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 83.toByte(),
    0.toByte(), 116.toByte(), 0.toByte(), 97.toByte(), 0.toByte(), 114.toByte(), 0.toByte(), 9.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 5.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 83.toByte(), 0.toByte(), 108.toByte(), 0.toByte(), 97.toByte(), 0.toByte(), 115.toByte(),
    0.toByte(), 104.toByte(), 0.toByte(), 10.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(),
    0.toByte(), 9.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 66.toByte(), 0.toByte(), 97.toByte(),
    0.toByte(), 99.toByte(), 0.toByte(), 107.toByte(), 0.toByte(), 83.toByte(), 0.toByte(), 108.toByte(),
    0.toByte(), 97.toByte(), 0.toByte(), 115.toByte(), 0.toByte(), 104.toByte(), 0.toByte(), 11.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 4.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 80.toByte(), 0.toByte(), 108.toByte(), 0.toByte(), 117.toByte(), 0.toByte(), 115.toByte(),
    0.toByte(), 12.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 5.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 77.toByte(), 0.toByte(), 105.toByte(), 0.toByte(), 110.toByte(),
    0.toByte(), 117.toByte(), 0.toByte(), 115.toByte(), 0.toByte(), 13.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 1.toByte(), 0.toByte(), 5.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 70.toByte(),
    0.toByte(), 108.toByte(), 0.toByte(), 111.toByte(), 0.toByte(), 97.toByte(), 0.toByte(), 116.toByte(),
    0.toByte(), 14.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 5.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 83.toByte(), 0.toByte(), 116.toByte(), 0.toByte(), 97.toByte(),
    0.toByte(), 114.toByte(), 0.toByte(), 50.toByte(), 0.toByte(), 15.toByte(), 0.toByte(), 0.toByte(),
    0.toByte(), 1.toByte(), 0.toByte(), 6.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 66.toByte(),
    0.toByte(), 105.toByte(), 0.toByte(), 110.toByte(), 0.toByte(), 73.toByte(), 0.toByte(), 110.toByte(),
    0.toByte(), 116.toByte(), 0.toByte(), 16.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(),
    0.toByte(), 6.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 79.toByte(), 0.toByte(), 99.toByte(),
    0.toByte(), 116.toByte(), 0.toByte(), 73.toByte(), 0.toByte(), 110.toByte(), 0.toByte(), 116.toByte(),
    0.toByte(), 17.toByte(), 0.toByte(), 0.toByte(), 0.toByte(), 1.toByte(), 0.toByte(), 6.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 72.toByte(), 0.toByte(), 101.toByte(), 0.toByte(), 120.toByte(),
    0.toByte(), 73.toByte(), 0.toByte(), 110.toByte(), 0.toByte(), 116.toByte(), 0.toByte(), 18.toByte(),
    0.toByte(), 0.toByte(), 0.toByte(), 0.toByte())
