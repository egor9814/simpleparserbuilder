package sample.ast

class VarExpr(
    val name: String
) : Expression() {

    override fun toString(): String {
        return name
    }

    override fun <A> apply(v: Visitor<A>, arg: A) {
        v.visit(this, arg)
    }

}
