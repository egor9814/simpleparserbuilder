package sample.ast

class NumberExpr(
    val value: String,
    val type: Type
) : Expression() {
    enum class Type {
        Int,
        HexInt,
        OctInt,
        BinInt,
        Float
    }

    override fun toString(): String {
        return "(${type.name}) $value"
    }

    override fun <A> apply(v: Visitor<A>, arg: A) {
        v.visit(this, arg)
    }

}
