package sample.ast

class CompilationUnit(
    val expressions: List<Expression>
) : Node() {

    override fun toString(): String {
        return expressions.joinToString(separator = "\n")
    }

    override fun <A> apply(v: Visitor<A>, arg: A) {
        v.visit(this, arg)
    }

}
