package sample.ast

class BinaryExpr(
    val left: Expression,
    val right: Expression,
    val o: Operator
) : Expression() {

    enum class Operator(val representation: String) {
        Add("+"),
        Sub("-"),
        Mul("*"),
        Pow("**"),
        Div("/"),
        WDiv("\\");

        companion object {
            fun of(representation: String) = values().find { it.representation == representation }
        }
    }

    override fun toString(): String {
        return "($left ${o.representation} $right)"
    }

    override fun <A> apply(v: Visitor<A>, arg: A) {
        v.visit(this, arg)
    }

}
