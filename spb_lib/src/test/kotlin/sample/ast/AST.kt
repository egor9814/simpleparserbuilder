package sample.ast

import ru.egor9814.spb.lib.parser.INode
import ru.egor9814.spb.lib.parser.Node
import ru.egor9814.spb.lib.parser.TokenNode
import sample.SampleLangParser.*
import sample.SampleLangTokenType
import kotlin.RuntimeException

fun IdNode.toExpr(): Expression {
    require(size == 1)
    val c = first()
    require(c is TokenNode)
    return VarExpr(c.value.value).apply {
        position = c.value.startPosition .. c.value.endPosition
    }
}

fun IntNode.toExpr(): Expression {
    require(size == 1)
    val c = first()
    require(c is TokenNode)
    return when (c.value.symbol.tokenType) {
        SampleLangTokenType.Int -> NumberExpr(c.value.value, NumberExpr.Type.Int)
        SampleLangTokenType.HexInt -> NumberExpr(c.value.value, NumberExpr.Type.HexInt)
        SampleLangTokenType.OctInt -> NumberExpr(c.value.value, NumberExpr.Type.OctInt)
        SampleLangTokenType.BinInt -> NumberExpr(c.value.value, NumberExpr.Type.BinInt)
        else -> throw RuntimeException("invalid token type: ${c.value}")
    }.apply {
        position = c.value.startPosition .. c.value.endPosition
    }
}

fun FloatNode.toExpr(): Expression {
    require(size == 1)
    val c = first()
    require(c is TokenNode)
    require(c.value.symbol.tokenType == SampleLangTokenType.Float)
    return NumberExpr(c.value.value, NumberExpr.Type.Float).apply {
        position = c.value.startPosition .. c.value.endPosition
    }
}

fun LiteralNode.toExpr(): Expression {
    require(size == 1)
    return when (val c = first()) {
        is IntNode -> c.toExpr()
        is FloatNode -> c.toExpr()
        else -> throw RuntimeException("invalid literal type")
    }
}

fun SimpleNode.toExpr(): Expression {
    require(isNotEmpty())
    return when (val c = first()) {
        is LiteralNode -> c.toExpr()
        is IdNode -> c.toExpr()
        is TokenNode -> {
            require(size == 3)
            require(c.value.symbol.tokenType == SampleLangTokenType.Lparen)
            require(children[2].let { it is TokenNode && it.value.symbol.tokenType == SampleLangTokenType.Rparen })
            val expr = children[1]
            require(expr is ExprNode)
            expr.toExpr()
        }
        else -> throw RuntimeException("invalid simple expression type")
    }
}

fun CallArgsNode.toArgs(): List<Expression> {
    val result = mutableListOf<Expression>()
    var nextIsExpression = true
    for (c in this) {
        if (nextIsExpression) {
            require(c is ExprNode)
            result.add(c.toExpr())
        } else {
            require(c is TokenNode && c.value.symbol.tokenType == SampleLangTokenType.Comma)
        }
        nextIsExpression = !nextIsExpression
    }
    if (nextIsExpression)
        throw RuntimeException("${result.lastOrNull()?.position?.end ?: "[?]"} expected expression after ','")
    return result.toList()
}

fun CallNode.toExpr(): Expression {
    require(isNotEmpty())
    val c = first()
    require(c is SimpleNode)
    var res = c.toExpr()

    var nextIsExpression = false
    var nextIsClose = false
    var callArgs = emptyList<Expression>()
    for (it in drop(1)) {
        when {
            nextIsExpression -> {
                if (it is TokenNode && it.value.symbol.tokenType == SampleLangTokenType.Rparen) {
                    res = CallExpr(res, callArgs)
                } else {
                    require(it is CallArgsNode)
                    callArgs = it.toArgs()
                    nextIsClose = true
                }
                nextIsExpression = false
            }
            nextIsClose -> {
                require(it is TokenNode && it.value.symbol.tokenType == SampleLangTokenType.Rparen)
                nextIsClose = false
                res = CallExpr(res, callArgs)
            }
            else -> {
                require(it is TokenNode && it.value.symbol.tokenType == SampleLangTokenType.Lparen)
                nextIsExpression = true
            }
        }
    }

    if (nextIsExpression) {
        throw RuntimeException("${res.position?.end ?: ""} expected call args or ')' after '('")
    }
    if (nextIsClose) {
        throw RuntimeException("${res.position?.end ?: ""} expected ')' after expression")
    }

    return res
}

fun Node.toBinaryOper(): BinaryExpr.Operator? {
    require(size == 1)
    val c = first()
    require(c is TokenNode)
    return BinaryExpr.Operator.of(c.value.value)
}

fun MultOperatorNode.toOper() = toBinaryOper()

fun <C : Expression, O : BinaryExpr.Operator> Node.toBinaryExpr(child: (INode) -> C, operator: (INode) -> O?): Expression {
    require(isNotEmpty())
    var res: Expression = first().let {
        child(it)
    }
    var o: BinaryExpr.Operator? = null
    for (c in drop(1)) {
        if (o == null) {
            o = operator(c)!!
        } else {
            res = BinaryExpr(res, child(c), o).apply {
                left.parent = this
                right.parent = this
                if (left.position != null && right.position != null) {
                    position = left.position!!.copy(end = right.position!!.end)
                }
            }
            o = null
        }
    }
    if (o != null) {
        throw RuntimeException("${res.position?.end ?: ""} expected expression after '${o.representation}'")
    }
    return res
}

fun MultNode.toExpr() = toBinaryExpr(
    { require(it is CallNode); it.toExpr() },
    { require(it is MultOperatorNode); it.toOper() }
)

fun AddOperatorNode.toOper() = toBinaryOper()

fun AddNode.toExpr() = toBinaryExpr(
    { require(it is MultNode); it.toExpr() },
    { require(it is AddOperatorNode); it.toOper() }
)

fun ExprNode.toExpr(): Expression {
    require(isNotEmpty())
    val c = first()
    require(c is AddNode)
    return c.toExpr()
}

fun MainNode.toCompilationUnit(): CompilationUnit {
    val result = mutableListOf<Expression>()
    forEach {
        require(it is ExprNode)
        result.add(it.toExpr())
    }
    return CompilationUnit(result.toList()).apply {
        expressions.forEach {
            it.parent = this
        }
        if (expressions.isNotEmpty()) {
            val begin = expressions.first().position
            val end = expressions.last().position
            if (begin != null && end != null) {
                position = begin.copy(end = end.end)
            }
        }
    }
}
