package sample.ast

import java.io.PrintStream

class ASMVisitor : Visitor<PrintStream> {

    companion object {
        fun asm(node: Node, out: PrintStream) {
            node.apply(ASMVisitor(), out)
        }
    }

    override fun visit(e: VarExpr, arg: PrintStream) {
        arg.println("load ${e.name}")
    }

    override fun visit(e: NumberExpr, arg: PrintStream) {
        arg.println("load_const ${e.type.name.toLowerCase()} ${e.value}")
    }

    override fun visit(e: BinaryExpr, arg: PrintStream) {
        e.left.apply(this, arg)
        e.right.apply(this, arg)
        arg.println(e.o.name.toLowerCase())
    }

    override fun visit(e: CallExpr, arg: PrintStream) {
        e.args.forEach {
            it.apply(this, arg)
        }
        e.target.apply(this, arg)
        arg.println("call ${e.args.size}")
    }

    override fun visit(u: CompilationUnit, arg: PrintStream) {
        u.expressions.forEach {
            it.apply(this, arg)
        }
    }

}
