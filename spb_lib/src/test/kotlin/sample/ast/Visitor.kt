package sample.ast

interface Visitor<A> {

    fun visit(e: VarExpr, arg: A)
    fun visit(e: NumberExpr, arg: A)
    fun visit(e: BinaryExpr, arg: A)
    fun visit(e: CallExpr, arg: A)

    fun visit(u: CompilationUnit, arg: A)

}
