package sample.ast

class CallExpr(
    val target: Expression,
    val args: List<Expression>
) : Expression() {

    override fun toString(): String {
        return "$target(${args.joinToString()})"
    }

    override fun <A> apply(v: Visitor<A>, arg: A) {
        v.visit(this, arg)
    }

}
