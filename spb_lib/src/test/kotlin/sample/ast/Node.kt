package sample.ast

import ru.egor9814.spb.lib.common.PositionRange

abstract class Node : Visitable {

    var parent: Node? = null
    var position: PositionRange? = null

}
