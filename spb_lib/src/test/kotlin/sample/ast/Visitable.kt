package sample.ast

interface Visitable {

    fun <A> apply(v: Visitor<A>, arg: A)

}
