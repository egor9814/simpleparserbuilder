/* Auto-Generated Recursive Descent Parser by egor9814 */

package sample

import ru.egor9814.spb.lib.lexer.*

object SampleLangTokenType : TokenTypeProvider() {
    val EOF =       object : SimpleTokenType("EOF", 0) {}
    val ERROR =     object : SimpleTokenType("ERROR", 1) {}
    val Ws =        object : SimpleTokenType("Ws", 2) {}
    val Id =        object : SimpleTokenType("Id", 3) {}
    val Int =       object : SimpleTokenType("Int", 4) {}
    val HexInt =    object : SimpleTokenType("HexInt", 5) {}
    val OctInt =    object : SimpleTokenType("OctInt", 6) {}
    val BinInt =    object : SimpleTokenType("BinInt", 7) {}
    val Float =     object : SimpleTokenType("Float", 8) {}
    val Lparen =    object : SimpleTokenType("Lparen", 9) {}
    val Rparen =    object : SimpleTokenType("Rparen", 10) {}
    val Comma =     object : SimpleTokenType("Comma", 11) {}
    val Star =      object : SimpleTokenType("Star", 12) {}
    val Star2 =     object : SimpleTokenType("Star2", 13) {}
    val Slash =     object : SimpleTokenType("Slash", 14) {}
    val BackSlash = object : SimpleTokenType("BackSlash", 15) {}
    val Plus =      object : SimpleTokenType("Plus", 16) {}
    val Minus =     object : SimpleTokenType("Minus", 17) {}
    init {
        register(EOF)
        register(ERROR)
        register(Ws)
        register(Id)
        register(Int)
        register(HexInt)
        register(OctInt)
        register(BinInt)
        register(Float)
        register(Lparen)
        register(Rparen)
        register(Comma)
        register(Star)
        register(Star2)
        register(Slash)
        register(BackSlash)
        register(Plus)
        register(Minus)
    }
}
