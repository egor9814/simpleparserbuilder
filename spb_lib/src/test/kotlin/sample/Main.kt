package sample

import ru.egor9814.spb.lib.parser.ParseResult
import ru.egor9814.spb.lib.parser.PrettyPrintNodeTree
import ru.egor9814.spb.lib.lexer.SourceCode
import sample.ast.ASMVisitor
import sample.ast.toCompilationUnit

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val src = SourceCode(
            "get * (smart - tv(3)) / 101.0  + 0xFf \\ 3f"
        )

        val lexer = SampleLangLexer(src)
        val tokens = lexer.tokenize()

        val parser = SampleLangParser(tokens)

        val result = parser.parse()

        if (result is ParseResult.Success) {
            println("Parse tree:\n${result.node}\n")

            println("Pretty print:")
            PrettyPrintNodeTree.print(result.node, System.out)
            println()

            result.castOrNull<SampleLangParser.MainNode>()?.apply {
                println("\nAST:")
                val ast = this.toCompilationUnit()
                println("$ast\n")

                println("ASM:")
                ASMVisitor.asm(ast, System.out)

                println()
            } ?: println("node is not Main!")
        } else if (result is ParseResult.Failed) {
            error(result.message)
        }
    }
}
